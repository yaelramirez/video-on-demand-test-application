"use strict";

import                  '../scss/style.scss';
import Navbar       from './components/Navbar.js';
import Home         from './components/Home.js';
import Details      from './components/Details.js';
import History      from './components/History.js';

import Utility      from './services/Utility.js';

const routes = {
    '/': Home, 
    '/details/:title': Details,
    '/history': History
}

const router = async () => {

    const header = null || document.getElementById('header');
    const content = null || document.getElementById('container');

    header.innerHTML = await Navbar.render();    

    let request = Utility.parseRequestURL();

    let parsedURL = (request.resource ? '/' + request.resource : '/') + (request.title ? '/:title' : '');
    
    let page = routes[parsedURL];
    content.innerHTML = await page.render();
    await page.after_render();
}

window.addEventListener('hashchange', router);

window.addEventListener('load', router);