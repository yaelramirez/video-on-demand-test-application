let getMovies = async () => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    try{
        const response = await fetch(`https://demo5520281.mockable.io/movies`, options);
        const json = await response.json();
        return json.entries;

    } catch(error){
        console.log('Error getting the data', error);
    }
}

let setUpEventListeners = () => {
    let currentItem = 0, offset = 0;

    let listItems = document.querySelectorAll('.movie-list li');
    listItems[currentItem].classList.add('active-movie');

        document.addEventListener('keyup', function(){

        let movieContainer = document.getElementById('movie-list');
        let widthElement = 0, rightPosition = 0, leftPosition = 0, total;

        switch(event.keyCode){
            case 39:   
                currentItem >= 19 ? currentItem = 19 : currentItem++;
                
                listItems[currentItem].classList.add('active-movie');
                listItems[currentItem-1].classList.remove('active-movie');

                rightPosition = document.getElementById(listItems[currentItem].id).getBoundingClientRect().right;
                widthElement = document.getElementById(listItems[currentItem].id).getBoundingClientRect().width;

                if(rightPosition >= window.innerWidth){
                    offset -= widthElement + 5;
                    movieContainer.style.transform = "translate(" + offset + "px)";
                }
            break;
            case 37:
                currentItem <= 0 ? currentItem = 0 : currentItem--;

                listItems[currentItem].classList.add('active-movie');
                listItems[currentItem + 1].classList.remove('active-movie');

                leftPosition = document.getElementById(listItems[currentItem].id).getBoundingClientRect().left;
                widthElement = document.getElementById(listItems[currentItem].id).getBoundingClientRect().width;
                
                total = Math.trunc(window.innerWidth / widthElement);

                if(total > currentItem + 1){
                    
                }else if(leftPosition <= window.innerWidth){
                    offset += widthElement + 5;
                    movieContainer.style.transform = "translate(" + offset + "px)";
                }
            break;
            case 13:
                    let movieTitle = listItems[currentItem].getElementsByTagName('p')[0].innerHTML;
                    let url = movieTitle.split(" ").join("-");
                    window.location = "#/details/" + url;
                break;
        }
    });
}

let Home = {
    render: async () => {
        
        let movies = await getMovies();
        let movieId = 1;

        let output = '<ul class="movie-list" id="movie-list">';
            for(let movie in movies){
                output += `
                    <li class="movie-item" id="${movieId++}">
                        <img src="${movies[movie].images[0].url}" style="height: 100%; width: 100%;"></img>
                        <p>${movies[movie].title}</p>
                    </li>
                `;
            }
        output += '</ul>';
        
        let view =  `
            <div class="movie-list-container">
                ${output}
            </div>
        `
        return view;
    }, 
    after_render: async () => {
        setUpEventListeners();
     }

}

export default Home;