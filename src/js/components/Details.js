import Utility       from '../services/Utility.js';

let getMovies = async () => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    try{
        const response = await fetch(`https://demo5520281.mockable.io/movies`, options);
        const json = await response.json();
        return json.entries;

    } catch(error){
        console.log('Error getting the data', error);
    }
}

let Details = {
    render : async () => {
        let view = `
        <section class="section">
            <h1> Detalles </h1>
            <h2> Estas en la pagina de detalles</h2>
        </section>
        `
        return view;
    }, 
    after_render: async () => { }

}

export default Details;