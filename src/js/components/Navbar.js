let Navbar = {
    render: async () => {
        let view =  `
            <nav class="nav">
                <div class="right-nav">
                    <ul class="navbar-items">
                        <li><a href="/#/">Home</a></li>
                        <li><a href="/#/history">History</a></li>
                    </ul>
                </div>
            </nav>
        `
        return view;
    },
    after_render: async () => { }
}

export default Navbar;