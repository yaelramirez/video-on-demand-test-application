const Utility = { 
   
    parseRequestURL: () => {
        let url = location.hash.slice(1) || '/';
        let urlComponent = url.split("/")
        let request = {
            resource    : null,
            title       : null
        }
        request.resource    = urlComponent[1]
        request.title       = urlComponent[2]

        return request;
    }
}

export default Utility;